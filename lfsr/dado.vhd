----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:38:28 12/27/2015 
-- Design Name: 
-- Module Name:    dado - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.math_real.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dado is
    Port ( play : in  STD_LOGIC;
           R : in  STD_LOGIC;
			  clk : in std_logic;
           num : out  STD_LOGIC_VECTOR (3 downto 0));
end dado;

architecture Behavioral of dado is
	COMPONENT lfsr
		PORT(
			clk : IN std_logic;
			R : IN std_logic;          
			data : OUT std_logic_vector(15 downto 0)
			);
		END COMPONENT;

	signal temp : std_logic_vector(15 downto 0);
	signal dado_temp : std_logic_vector(3 downto 0) := "0000";
begin
	Inst_lfsr: lfsr PORT MAP(
		clk => clk,
		R => R,
		data => temp
	);
	num <= dado_temp;
	process(play)
	variable value : integer range 0 to 65536:=65536;
	begin
		if rising_edge(play) and R = '0' then
			value := to_integer(unsigned(temp));
		elsif rising_edge(clk) and R = '1' then
			dado_temp <= "0000";
		end if;
		
		if value <= 10922 then
				dado_temp <= "0001";
		elsif value > 10922 and value<=21844 then 
				dado_temp <= "0010";
		elsif value > 21844 and value<=32767 then
				dado_temp <= "0011";
		elsif value > 32767 and value<=43689 then
				dado_temp <= "0100";
		elsif value > 43689 and value<=54611 then
				dado_temp <= "0101";
		elsif value = 65536 then 
				dado_temp <= "0000";
		else
				dado_temp <= "0110";
		end if;
	
	end process;
end Behavioral;

