/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/daaguirre/lfsr/dado.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_1242562249;

int ieee_p_1242562249_sub_17802405650254020620_1035706684(char *, char *, char *);
unsigned char ieee_p_2592010699_sub_2763492388968962707_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_0384206778_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(57, ng0);

LAB3:    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t1 = (t0 + 3768);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 4U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 3672);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0384206778_3212880686_p_1(char *t0)
{
    unsigned char t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    unsigned char t6;
    unsigned char t7;
    char *t8;
    int t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    int t15;

LAB0:    xsi_set_current_line(61, ng0);
    t2 = (t0 + 992U);
    t3 = ieee_p_2592010699_sub_2763492388968962707_503743352(IEEE_P_2592010699, t2, 0U, 0U);
    if (t3 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:    t2 = (t0 + 1312U);
    t3 = ieee_p_2592010699_sub_2763492388968962707_503743352(IEEE_P_2592010699, t2, 0U, 0U);
    if (t3 == 1)
        goto LAB10;

LAB11:    t1 = (unsigned char)0;

LAB12:    if (t1 != 0)
        goto LAB8;

LAB9:
LAB3:    xsi_set_current_line(67, ng0);
    t2 = (t0 + 2128U);
    t4 = *((char **)t2);
    t9 = *((int *)t4);
    t1 = (t9 <= 10922);
    if (t1 != 0)
        goto LAB13;

LAB15:    t2 = (t0 + 2128U);
    t4 = *((char **)t2);
    t9 = *((int *)t4);
    t3 = (t9 > 10922);
    if (t3 == 1)
        goto LAB18;

LAB19:    t1 = (unsigned char)0;

LAB20:    if (t1 != 0)
        goto LAB16;

LAB17:    t2 = (t0 + 2128U);
    t4 = *((char **)t2);
    t9 = *((int *)t4);
    t3 = (t9 > 21844);
    if (t3 == 1)
        goto LAB23;

LAB24:    t1 = (unsigned char)0;

LAB25:    if (t1 != 0)
        goto LAB21;

LAB22:    t2 = (t0 + 2128U);
    t4 = *((char **)t2);
    t9 = *((int *)t4);
    t3 = (t9 > 32767);
    if (t3 == 1)
        goto LAB28;

LAB29:    t1 = (unsigned char)0;

LAB30:    if (t1 != 0)
        goto LAB26;

LAB27:    t2 = (t0 + 2128U);
    t4 = *((char **)t2);
    t9 = *((int *)t4);
    t3 = (t9 > 43689);
    if (t3 == 1)
        goto LAB33;

LAB34:    t1 = (unsigned char)0;

LAB35:    if (t1 != 0)
        goto LAB31;

LAB32:    t2 = (t0 + 2128U);
    t4 = *((char **)t2);
    t9 = *((int *)t4);
    t1 = (t9 == 65536);
    if (t1 != 0)
        goto LAB36;

LAB37:    xsi_set_current_line(80, ng0);
    t2 = (t0 + 5800);
    t5 = (t0 + 3832);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t2, 4U);
    xsi_driver_first_trans_fast(t5);

LAB14:    t2 = (t0 + 3688);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(62, ng0);
    t4 = (t0 + 1672U);
    t8 = *((char **)t4);
    t4 = (t0 + 5688U);
    t9 = ieee_p_1242562249_sub_17802405650254020620_1035706684(IEEE_P_1242562249, t8, t4);
    t10 = (t0 + 2128U);
    t11 = *((char **)t10);
    t10 = (t11 + 0);
    *((int *)t10) = t9;
    goto LAB3;

LAB5:    t4 = (t0 + 1192U);
    t5 = *((char **)t4);
    t6 = *((unsigned char *)t5);
    t7 = (t6 == (unsigned char)2);
    t1 = t7;
    goto LAB7;

LAB8:    xsi_set_current_line(64, ng0);
    t4 = (t0 + 5772);
    t10 = (t0 + 3832);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t4, 4U);
    xsi_driver_first_trans_fast(t10);
    goto LAB3;

LAB10:    t4 = (t0 + 1192U);
    t5 = *((char **)t4);
    t6 = *((unsigned char *)t5);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB12;

LAB13:    xsi_set_current_line(68, ng0);
    t2 = (t0 + 5776);
    t8 = (t0 + 3832);
    t10 = (t8 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_fast(t8);
    goto LAB14;

LAB16:    xsi_set_current_line(70, ng0);
    t2 = (t0 + 5780);
    t10 = (t0 + 3832);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t2, 4U);
    xsi_driver_first_trans_fast(t10);
    goto LAB14;

LAB18:    t2 = (t0 + 2128U);
    t5 = *((char **)t2);
    t15 = *((int *)t5);
    t6 = (t15 <= 21844);
    t1 = t6;
    goto LAB20;

LAB21:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 5784);
    t10 = (t0 + 3832);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t2, 4U);
    xsi_driver_first_trans_fast(t10);
    goto LAB14;

LAB23:    t2 = (t0 + 2128U);
    t5 = *((char **)t2);
    t15 = *((int *)t5);
    t6 = (t15 <= 32767);
    t1 = t6;
    goto LAB25;

LAB26:    xsi_set_current_line(74, ng0);
    t2 = (t0 + 5788);
    t10 = (t0 + 3832);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t2, 4U);
    xsi_driver_first_trans_fast(t10);
    goto LAB14;

LAB28:    t2 = (t0 + 2128U);
    t5 = *((char **)t2);
    t15 = *((int *)t5);
    t6 = (t15 <= 43689);
    t1 = t6;
    goto LAB30;

LAB31:    xsi_set_current_line(76, ng0);
    t2 = (t0 + 5792);
    t10 = (t0 + 3832);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t2, 4U);
    xsi_driver_first_trans_fast(t10);
    goto LAB14;

LAB33:    t2 = (t0 + 2128U);
    t5 = *((char **)t2);
    t15 = *((int *)t5);
    t6 = (t15 <= 54611);
    t1 = t6;
    goto LAB35;

LAB36:    xsi_set_current_line(78, ng0);
    t2 = (t0 + 5796);
    t8 = (t0 + 3832);
    t10 = (t8 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_fast(t8);
    goto LAB14;

}


extern void work_a_0384206778_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0384206778_3212880686_p_0,(void *)work_a_0384206778_3212880686_p_1};
	xsi_register_didat("work_a_0384206778_3212880686", "isim/dado_tb_isim_beh.exe.sim/work/a_0384206778_3212880686.didat");
	xsi_register_executes(pe);
}
