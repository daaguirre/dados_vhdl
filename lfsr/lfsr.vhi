
-- VHDL Instantiation Created from source file lfsr.vhd -- 19:36:55 12/27/2015
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT lfsr
	PORT(
		clk : IN std_logic;
		R : IN std_logic;          
		data : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;

	Inst_lfsr: lfsr PORT MAP(
		clk => ,
		R => ,
		data => 
	);


