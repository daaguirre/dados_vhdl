--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:13:52 12/28/2015
-- Design Name:   
-- Module Name:   /home/daaguirre/lfsr/dado_tb.vhd
-- Project Name:  lfsr
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: dado
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY dado_tb IS
END dado_tb;
 
ARCHITECTURE behavior OF dado_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT dado
    PORT(
         play : IN  std_logic;
         R : IN  std_logic;
         clk : IN  std_logic;
         num : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal play : std_logic := '0';
   signal R : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal num : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
	constant play_wait :time := 7 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: dado PORT MAP (
          play => play,
          R => R,
          clk => clk,
          num => num
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
	
	play_process : process
	begin
		wait for play_wait;
		play <= '0'; 
		wait for clk_period*2;
		play <= '1'; 
		wait for clk_period*2;
		wait for play_wait;
	end process;

   -- Stimulus process
   stim_proc: process
   begin		      
		wait for clk_period*50;
      -- insert stimulus here 
		
		assert false
			report "Simulación finalizada"
			severity failure;
   end process;

END;
